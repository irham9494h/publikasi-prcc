<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToOpdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('opds', function (Blueprint $table) {
           $table->text('alamat');
           $table->string('email');
           $table->string('facebook');
           $table->string('instagram');
           $table->string('twitter');
           $table->string('telegram');
           $table->string('hp_pic');
           $table->string('hp_instansi');
           $table->string('foto_pic');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('opds', function (Blueprint $table) {
            $table->dropColumn('alamat');
            $table->dropColumn('email');
            $table->dropColumn('facebook');
            $table->dropColumn('instagram');
            $table->dropColumn('twitter');
            $table->dropColumn('telegram');
            $table->dropColumn('hp_pic');
            $table->dropColumn('hp_instansi');
            $table->dropColumn('foto_pic');
        });
    }
}
