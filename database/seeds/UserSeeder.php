<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'nama' => 'Administrator',
                'username' => 'admin',
                'email' => 'admin@app.com',
                'password' => Hash::make('123456'),
                'role' => 'admin'
            ],
            [
                'nama' => 'Bagus Arya',
                'username' => 'bagus',
                'email' => 'bagus@app.com',
                'password' => Hash::make('123456'),
                'role' => 'user'
            ],
            [
                'nama' => 'Rita Rahma',
                'username' => 'rita',
                'email' => 'rita@app.com',
                'password' => Hash::make('123456'),
                'role' => 'user'
            ]
        ];

        foreach ($data as $user) {
            \App\User::create($user);
        }
    }
}
