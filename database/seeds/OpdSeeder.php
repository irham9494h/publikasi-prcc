<?php

use Illuminate\Database\Seeder;

class OpdSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            'Biro Pemerintahan',
            'Biro Hukum',
            'Biro Organisasi',
            'Biro Administrasi Pembangunan dan LPBJP',
            'Biro Administrasi Kesejateraan Rakyat',
            'Biro Umum',
            'Biro Humas dan Protokol',
            'Biro Kerjasama',
            'Biro Ekonomi',
            'Sekretaris DPRD Prov.NTB',
            'Dinas Pendidikan clanKebudayaan Prov.NTB',
            'Dinas Perumahan dan Pemukiman Prov.NTB',
            'Dinas Kelautan clan Perikanan Prov.NTB',
            'Dinas Kesehatan Prov.NTB',
            'Dinas Koperasi Usaha Mikro Kecil dan Menengah Prov.NTB',
            'Dinas Pekerjaan Umum dan Tata Ruang Prov.NTB',
            'Dinas Pemberdayaan Masyarakat, Pemdes, Kependudukan dan Pencatatan Sipil Prov.NTB',
            'Dinas Pemuda dan Olahraga Prov.NTB',
            'Dinas Perhubungan Prov.NTB',
            'Dinas Perindustrian Prov.NTB',
            'Dinas Perdagangan Prov.NTB',
            'Dinas Energi dan Sumber Daya Mineral Prov.NTB',
            'Dinas Pertanian dan Perkebunan Prov.NTB',
            'Dinas Petemakan dan Kesehatan Hewan Prov.NTB',
            'Dinas Sosial Prov.NTB',
            'Dinas Pemberdayaan Perempuan, Perlindungan Anak, Pengendalian Penduduk dan Keluarga Berencana Prov.NTB',
            'Dinas Ketahanan Pangan Prov.NTB',
            'Dinas Komunikasi, Informatika dan Statistik Prov.NTB',
            'Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Prov.NTB',
            'Dinas Lingkungan Hidup dan Kehutanan',
            'Dinas Perpustakaan Dinas dan Arsip',
            'Dinas Pariwisata Prov.NTB',
            'Dinas Tenaga Keija dan Transmigrasi Prov.NTB',
            'Badan Kepegawaian Daerah Prov.NTB',
            'Bakesbangpoldagri Prov.NTB',
            'Badan Pengelola Keuangan dan Aset Daerah Prov.NTB',
            'Inspektorat Prov.NTB',
            'Rumah Sakit Umum Provinsi NTB',
            'Badan Perencanaan Pembangunan, Penelitian dan Pengembangan Daerah Prov.NTB',
            'Kepala Badan Pengelola Pendapatan Daerah Prov. NTB',
            'Badan Penanggulangan Bencana Daerah Prov. NTB',
            'Badan Pengembangan Sumber Daya Manusia Daerah Prov. NTB',
            'Satpol PP Prov. NTB',
            'Rumah Sakit Jiwa Mutiara Sukma Prov. NTB',
            'Badan Penghubungan NTB',
            'BIN Daerah NTB',
            'BNN Prov. NTB',
            'Kantor Wilayah Ditjen Perbendaharaan Prov. NTB',
            'Kantor Wilayah Kementrian Agama Prov. NTB',
            'Kantor Wilayah Ditjen Pajak Prov. NTB',
            'Badan Pemeriksa Keuangan Perwakilan NTB',
            'Pengadilan Tinggi Agama Prov. NTB',
            'Badan Pusat Statistik Prov. NTB',
            'Kantor Kemenkum dan Ham Prov. NTB',
            'Bulog Divre NTB',
            'KPU Prov. NTB',
            'Bawaslu Prov. NTB',
            'Komisi Informasi Prov. NTB',
            'Komisi Penyiaran Indonesia Prov. NTB',
            'Kantor Perwakilan Bank Indonesia Prov. NTB',
            'OJK NTB',
            'Perwakilan Kemenham RI Prov. NTB',
            'Perwakilan Ombudsman RI Prov. NTB',
            'Pertahanan Nasional NTB',
            'Bank Indonesia NTB',
            'Bank BNI Mataram',
            'Bank BRI Mataram',
            'Bank Mandiri Mataram',
            'Bank BTN',
            'Bank NTB Syariah Mataram',
            'PT. Telkomunikasi Mataram',
            'PT. PLN Mataram',
            'PT. Pos Indonesia Mataram',
            'PT. Angkasa Pura 1 Mataram',
            'PT. Newmont Nusa Tenggara',
            'PT. Pelindo Indonesia III Cabang Lembar',
            'PT. Pelindo Indonesia III Cabang Kayangan',
            'PT. Garuda Indonesia',
            'PT. Pelni Mataram',
            'Direktur Perum Damri Mataram',
            'PDAM Giri Menang Mataram',
        );

        foreach ($data as $opd) {
            \App\Opd::create([
                'nama' => $opd,
                'alamat' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'email' => 'email@app.com',
                'hp_pic' => '123456789',
                'hp_instansi' => '123456789'
            ]);
        }
    }
}
