<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dev', 'Dev\RouteController@index')->name('dev');
Route::get('/dev/filter', 'Dev\RouteController@filter')->name('filter.user-opd');

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/user/logout', 'Auth\LoginController@logout')->name('user.logout');
Route::post('/user/register/', 'Auth\LoginController@register')->name('user.register');
Auth::routes();

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function () {
    Route::get('/', 'Admin\AdminController@index')->name('admin');

    Route::get('/user-manajemen/delete/{user_manajeman}', 'Admin\UserController@destroy')->name('user-manajemen.delete');
    Route::resource('/user-manajemen', 'Admin\UserController');

    Route::get('/daftar-kegiatan/delete/{daftar_kegiatan}', 'Admin\KegiatanController@destroy')->name('daftar-kegiatan.delete');
    Route::get('/daftar-kegiatan/show-by-date/', 'Admin\KegiatanController@showByDate')->name('daftar-kegiatan.show-by-date');
    Route::get('/daftar-kegiatan/show-by-opd/', 'Admin\KegiatanController@showByOpd')->name('daftar-kegiatan.show-by-opd');
    Route::resource('/daftar-kegiatan', 'Admin\KegiatanController');

    Route::get('/opd-manajemen/delete/{opd_manajemen}', 'Admin\OpdController@destroy')->name('opd-manajemen.delete');
    Route::resource('/opd-manajemen', 'Admin\OpdController');
});

Route::group(['prefix' => 'opd', 'middleware' => ['auth', 'opd']], function () {
    Route::get('/', 'Opd\OpdController@index')->name('opd');
    Route::get('/profile/{id}', 'Opd\OpdController@profile')->name('opd.profile');
    Route::get('/profile/update/{id}/edit', 'Opd\OpdController@editProfile')->name('opd.edit-profile');
    Route::match(['put','patch'],'/profile/update/{id}', 'Opd\OpdController@updateProfile')->name('opd.update-profile');

    Route::get('/kegiatan/delete/{kegiatan}', 'Opd\KegiatanController@destroy')->name('kegiatan.delete');
    Route::get('/kegiatan/show-by-date/', 'Opd\KegiatanController@showByDate')->name('kegiatan.show-by-date');
    Route::resource('/kegiatan', 'Opd\KegiatanController');
});

