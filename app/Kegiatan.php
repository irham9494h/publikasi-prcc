<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kegiatan extends Model
{
    protected $guarded = [];

    public function photo()
    {
        return $this->hasMany(Photo::class);
    }

    public function opd()
    {
        return $this->belongsTo(Opd::class);
    }
}
