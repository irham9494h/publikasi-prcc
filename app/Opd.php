<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opd extends Model
{
    protected $guarded = [];

    public function kegiatan()
    {
        return $this->hasMany(Kegiatan::class);
    }

    public function user(){
        return $this->hasMany(User::class);
    }
}
