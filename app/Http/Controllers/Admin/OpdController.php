<?php

namespace App\Http\Controllers\Admin;

use App\Opd;
use Carbon\Carbon;
use File;
use Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OpdController extends Controller
{

    public $path;

    public function __construct()
    {
        $this->path = storage_path('app/public/photo-pic');
    }

    public function index()
    {
        $opds = Opd::with('user', 'kegiatan')->orderBy('nama', 'asc')->get();
        return view('pages.admin.opd.opd', compact('opds'));
//        return dd($opds);
    }

    public function create()
    {
        return view('pages.admin.opd.tambahOpd');
    }

    public function store(Request $request)
    {
//        if ($request->hasFile('image')) {
//
//            if (!File::isDirectory($this->path)) {
//                File::makeDirectory($this->path);
//            }
//            $file = $request->file('image');
//            $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
//            Image::make($file)->save($this->path . '/' . $fileName);

            $opd = Opd::create([
                'nama' => $request->nama,
                'email' => $request->email,
                'alamat' => $request->alamat,
                'facebook' => $request->facebook,
                'instagram' => $request->instagram,
                'twitter' => $request->twitter,
                'telegram' => $request->telegram,
                'hp_instansi' => $request->hp_instansi,
                'hp_pic' => $request->hp_pic,
//                'foto_pic' => 'photo-pic/'. $fileName,
            ]);
            if ($opd) {
                return redirect()->back()->with(['success' => 'Data OPD berhasil disimpan']);
            } else {
                return redirect()->back()->with(['error' => 'Data OPD gagal disimpan']);
            }
//        }

    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $opd = Opd::find($id);
        return view('pages.admin.opd.tambahOpd', compact('opd'));
    }

    public function update(Request $request, $id)
    {
        if ($request->hasFile('image')) {

            if (!File::isDirectory($this->path)) {
                File::makeDirectory($this->path);
            }
            $file = $request->file('image');
            $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
            Image::make($file)->save($this->path . '/' . $fileName);

            $opd = Opd::find($id);
            $opd = $opd->update([
                'nama' => $request->nama,
                'email' => $request->email,
                'alamat' => $request->alamat,
                'facebook' => $request->facebook,
                'instagram' => $request->instagram,
                'twitter' => $request->twitter,
                'telegram' => $request->telegram,
                'hp_instansi' => $request->hp_instansi,
                'hp_pic' => $request->hp_pic,
                'foto_pic' => 'photo-pic/'.$fileName,
            ]);
            if ($opd) {
                return redirect()->route('opd-manajemen.index')->with(['success' => 'Data OPD berhasil diubah']);
            } else {
                return redirect()->back()->with(['error' => 'Data OPD gagal diubah']);
            }
        } else {
            $opd = Opd::find($id);
            $opd = $opd->update([
                'nama' => $request->nama,
                'email' => $request->email,
                'alamat' => $request->alamat,
                'facebook' => $request->facebook,
                'instagram' => $request->instagram,
                'twitter' => $request->twitter,
                'telegram' => $request->telegram,
                'hp_instansi' => $request->hp_instansi,
                'hp_pic' => $request->hp_pic,
            ]);
            if ($opd) {
                return redirect()->route('opd-manajemen.index')->with(['success' => 'Data OPD berhasil diubah']);
            } else {
                return redirect()->back()->with(['error' => 'Data OPD gagal diubah']);
            }
        }
    }

    public function destroy($id)
    {
        $opd = Opd::findOrFail($id);
        $opd = $opd->delete();
        if ($opd){
            return redirect()->back()->with(['success' => 'Data OPD berhasil dihapus']);
        }else{
            return redirect()->back()->with(['error' => 'Data OPD gagal dihapus']);
        }
    }
}
