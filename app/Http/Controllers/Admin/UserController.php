<?php

namespace App\Http\Controllers\Admin;

use App\Opd;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $managementUser = User::with('opd')->orderBy('nama', 'asc')->where('role', '=', 'user')->get();
        return view('pages.admin.user.user', compact('managementUser'));
//                return dd($managementUser);
    }

    public function create()
    {
        $opds = Opd::orderBy('nama', 'asc')->get();
        return view('pages.admin.user.formUser', compact('opds'));
    }

    public function store(Request $request)
    {
        $data = User::create([
            'nama' => $request->nama,
            'email' => $request->email,
            'username' => $request->username,
            'password' => Hash::make($request->password),
            'role' => 'user',
            'opd_id' => $request->NamaOpd
        ]);

        if ($data) {
            return redirect()->back()->with(['success' => 'Data user berhasil disimpan']);
        } else {
            return redirect()->route('user-manajemen.index')->with(['success' => 'Data user berhasil disimpan']);
        }
    }

    public function show($id)
    {
        return dd($id);
    }

    public function edit($id)
    {
        $opds = Opd::orderBy('nama', 'asc')->get();
        $user = User::find($id);
        return view('pages.admin.user.formUser', compact('user', 'opds'));
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if ($request->has('password')){
            $status = $user->update([
                'nama' => $request->nama,
                'email' => $request->email,
                'username' => $request->username,
                'password' => Hash::make($request->password),
            ]);
        }else{
            $status = $user->update([
                'nama' => $request->nama,
                'email' => $request->email,
                'username' => $request->username,
            ]);
        }


        if ($status) {
            return redirect()->route('user-manajemen.index')->with('success', 'Data user berhasil diubah');
        } else {
            return redirect()->back()->with('error', 'Data user gagal diubah');
        }
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user = $user->delete();
        if ($user) {
            return redirect()->back()->with(['success' => 'Data user berhasil dihapus']);
        } else {
            return redirect()->back()->with(['error' => 'Data user gagal dihapus']);
        }
    }
}
