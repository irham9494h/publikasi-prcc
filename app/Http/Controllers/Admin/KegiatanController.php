<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Kegiatan;

class KegiatanController extends Controller
{
    public function index()
    {
        $res = Kegiatan::with('photo')
            ->orderBy('tanggal_kegiatan', 'desc')
            ->groupBy('tanggal_kegiatan')->paginate(5);
        $kegiatans = array();
        foreach ($res as $keg) {
            array_push($kegiatans, [
                'tanggal' => $keg->tanggal_kegiatan,
                'data' => Kegiatan::with('opd')->where('tanggal_kegiatan', '=', $keg->tanggal_kegiatan)->limit(4)->get(),
            ]);
        }
        return view('pages.admin.kegiatan.kegiatan', compact('kegiatans','res'));
//        return dd($kegiatans);
    }

    public function create()
    {
        $opds = Opd::orderBy('nama', 'asc')->get();
        return view('pages.admin.kegiatan.tambahKegiatan', compact('opds'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama_kegiatan' => 'required',
            'tanggal_kegiatan' => 'required',
            'lokasi' => 'required',
            'narasumber' => 'required',
            'agenda' => 'required',
            'hasil' => 'required',
        ]);

        if ($validatedData) {
            if ($request->has('opd_id')) {
                $data = Kegiatan::create([
                    'nama_kegiatan' => $request->nama_kegiatan,
                    'tanggal_kegiatan' => $request->tanggal_kegiatan,
                    'lokasi' => $request->lokasi,
                    'narasumber' => $request->narasumber,
                    'agenda' => $request->agenda,
                    'hasil' => $request->hasil,
                    'opd_id' => $request->opd_id
                ]);

                if ($data) {
                    return redirect()->back()->with(['status' => true, 'message' => 'Success']);
                }
            } else {
                $data = Kegiatan::create([
                    'nama_kegiatan' => $request->nama_kegiatan,
                    'tanggal_kegiatan' => $request->tanggal_kegiatan,
                    'lokasi' => $request->lokasi,
                    'narasumber' => $request->narasumber,
                    'agenda' => $request->agenda,
                    'hasil' => $request->hasil,
                ]);

                if ($data) {
                    return redirect()->back()->with(['status' => true, 'message' => 'Success']);
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kegiatan = Kegiatan::with('photo')->where('id', '=', $id)->first();
        return view('pages.admin.kegiatan.galleryKegiatan', compact('kegiatan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Kegiatan $kegiatan)
    {
        return dd($kegiatan);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kegiatan $kegiatan)
    {
        $validatedData = $request->validate([
            'nama_kegiatan' => 'required',
            'tanggal_kegiatan' => 'required',
            'lokasi' => 'required',
            'narasumber' => 'required',
            'agenda' => 'required',
            'hasil' => 'required',
        ]);

        if ($validatedData) {
            if ($request->has('opd_id')) {
                $data = Kegiatan::update([
                    'nama_kegiatan' => $request->nama_kegiatan,
                    'tanggal_kegiatan' => $request->tanggal_kegiatan,
                    'lokasi' => $request->lokasi,
                    'narasumber' => $request->narasumber,
                    'agenda' => $request->agenda,
                    'hasil' => $request->hasil,
                    'opd_id' => $request->opd_id
                ]);

                if ($data) {
                    return redirect()->back()->with(['status' => true, 'message' => 'Updated']);
                }
            } else {
                $data = Kegiatan::create([
                    'nama_kegiatan' => $request->nama_kegiatan,
                    'tanggal_kegiatan' => $request->tanggal_kegiatan,
                    'lokasi' => $request->lokasi,
                    'narasumber' => $request->narasumber,
                    'agenda' => $request->agenda,
                    'hasil' => $request->hasil,
                ]);

                if ($data) {
                    return redirect()->back()->with(['status' => true, 'message' => 'Updated']);
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kegiatan $kegiatan)
    {
        $kegiatan->delete();
        return redirect()->back()->with(['status' => true, 'message' => 'Deleted']);
    }

    public function showByDate(Request $request)
    {
        $kegiatans = Kegiatan::with('opd')->where('tanggal_kegiatan', '=', $request->tanggal)->orderBy('tanggal_kegiatan', 'desc')->paginate(8);
        return view('pages.admin.kegiatan.showKegiatanByDate', compact('kegiatans'));
//        return dd($kegiatans);
    }

    public function showByOpd(Request $request)
    {
        $kegiatans = Kegiatan::with('opd')->where('opd_id', '=', $request->opd)->orderBy('tanggal_kegiatan', 'desc')->paginate(8);
        return view('pages.admin.kegiatan.showKegiatanByOpd', compact('kegiatans'));
//        return dd($kegiatans);
    }
}
