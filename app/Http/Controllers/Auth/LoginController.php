<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    public function showLoginForm()
    {
        return view('pages.admin.auth.login');
    }

    public function login(Request $request){
        if (filter_var($request->username, FILTER_VALIDATE_EMAIL)) {
            $credentials = [
                'email' => $request->username,
                'password' => $request->password,
            ];
        } else {
            $credentials = [
                'username' => $request->username,
                'password' => $request->password,
            ];
        }

        if (Auth::attempt($credentials)) {
            if (Auth::user() &&  Auth::user()->role == 'admin') {
                return redirect()->route('admin');
            } else {
                return redirect()->route('opd');
            }
        }
        return redirect()->back()->with(['error' => 'login gagal']);
    }

    public function register(Request $request)
    {
        $reg = User::create([
            'nama' => $request->nama,
            'email' => $request->email,
            'username' => $request->username,
            'password' => Hash::make($request->password),
            'role' => 'user',
            'opd_id' => $request->opd_id
        ]);

        if ($reg) {
            return redirect()->route('login')->with(['success' => 'Data user berhasil disimpan']);
        } else {
            return redirect()->route('user-manajemen.index')->with(['success' => 'Data user berhasil disimpan']);
        }
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
