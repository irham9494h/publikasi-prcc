<?php

namespace App\Http\Controllers\Opd;

use App\Opd;
use Carbon\Carbon;
use Illuminate\Http\Request;
use File;
use Image;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class OpdController extends Controller
{
    public $path;

    public function __construct()
    {
        $this->path = storage_path('app/public/photo-pic');
    }

    public function index()
    {
//        return view('layout.main');
        return redirect()->route('kegiatan.index');
    }

    public function profile($id){
        $profil = Opd::findOrFail($id);
        return view('pages.opd.profil.profil', compact('profil'));
    }

    public function editProfile($id){
        $opd = Opd::findOrFail($id);
        return view('pages.opd.profil.updateProfile', compact('opd'));
    }

    public function updateProfile(Request $request, $id)
    {
        if ($request->hasFile('image')) {

            if (!File::isDirectory($this->path)) {
                File::makeDirectory($this->path);
            }
            $file = $request->file('image');
            $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
            Image::make($file)->save($this->path . '/' . $fileName);

            $opd = Opd::find($id);
            $opd = $opd->update([
                'nama' => $request->nama,
                'email' => $request->email,
                'alamat' => $request->alamat,
                'facebook' => $request->facebook,
                'instagram' => $request->instagram,
                'twitter' => $request->twitter,
                'telegram' => $request->telegram,
                'hp_instansi' => $request->hp_instansi,
                'hp_pic' => $request->hp_pic,
                'foto_pic' => 'photo-pic/'.$fileName,
            ]);
            if ($opd) {
                return redirect()->route('opd.profile', Auth::user()->opd->id)->with(['success' => 'Data OPD berhasil diubah']);
            } else {
                return redirect()->back()->with(['error' => 'Data OPD gagal diubah']);
            }
        } else {
            $opd = Opd::find($id);
            $opd = $opd->update([
                'nama' => $request->nama,
                'email' => $request->email,
                'alamat' => $request->alamat,
                'facebook' => $request->facebook,
                'instagram' => $request->instagram,
                'twitter' => $request->twitter,
                'telegram' => $request->telegram,
                'hp_instansi' => $request->hp_instansi,
                'hp_pic' => $request->hp_pic,
            ]);
            if ($opd) {
                return redirect()->route('opd.profile', Auth::user()->opd->id)->with(['success' => 'Data OPD berhasil diubah']);
            } else {
                return redirect()->back()->with(['error' => 'Data OPD gagal diubah']);
            }
        }
    }

}
