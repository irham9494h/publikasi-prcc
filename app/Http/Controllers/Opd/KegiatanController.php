<?php

namespace App\Http\Controllers\Opd;

use App\Kegiatan;
use App\Photo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Image;
use File;

class KegiatanController extends Controller
{
    public function index()
    {
        $res = Kegiatan::with('photo')
            ->where('opd_id', '=', Auth::user()->opd->id)
            ->orderBy('tanggal_kegiatan', 'desc')
            ->groupBy('tanggal_kegiatan')->paginate(5);
        $kegiatans = array();
        foreach ($res as $keg) {
            array_push($kegiatans, [
                'tanggal' => $keg->tanggal_kegiatan,
                'data' => Kegiatan::where('opd_id', '=', Auth::user()->opd->id)->where('tanggal_kegiatan', '=', $keg->tanggal_kegiatan)->limit(4)->get(),
            ]);
        }
        return view('pages.opd.kegiatan.kegiatan', compact('kegiatans', 'res'));
//        return dd($res);
    }

    public function create()
    {
        return view('pages.opd.kegiatan.formKegiatan');
    }

    public function store(Request $request)
    {

        $path = 'app/public/foto-kegiatan';
        $thumbnail = 'app/public/foto-kegiatan/thumbnail';

        // if ($request->hasFile('foto_kegiatan')) {

        if (!File::isDirectory(storage_path($path))) {
            File::makeDirectory(storage_path($path));
        }

        $file = $request->file('foto_kegiatan');
        $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
        Image::make($file)->save(storage_path($path) . '/' . $fileName);

        $canvas = Image::canvas(300, 200);
        $resizeImage = Image::make($file)->fit(300, 200);

        if (!File::isDirectory(storage_path($thumbnail))) {
            File::makeDirectory(storage_path($thumbnail));
        }

        $canvas->insert($resizeImage, 'center');
        $canvas->save(storage_path($thumbnail) . '/' . $fileName);

        $kegiatan = Kegiatan::create([
            'nama_kegiatan' => $request->NamaKegiatan,
            'tanggal_kegiatan' => $request->tanggal,
            'lokasi' => $request->lokasi,
            'narasumber' => $request->narasumber,
            'agenda' => $request->agenda,
            'hasil' => $request->hasil,
            'opd_id' => Auth::user()->opd->id,
        ]);

        if ($kegiatan) {
            $upload = Photo::create([
                'thumbnail' => 'foto-kegiatan/thumbnail/' . $fileName,
                'foto' => 'foto-kegiatan/' . $fileName,
                'kegiatan_id' => $kegiatan->id,
            ]);
            return redirect()->back()->with(['success' => 'Data kegiatan berhasil disimpan']);
        } else {
            return redirect()->back()->with(['error' => 'Data kegiatan gagal disimpan']);
        }
        // }
    }

    public function show($id)
    {
        $kegiatan = Kegiatan::with('photo')->where('id', '=', $id)->first();
        return view('pages.opd.kegiatan.galleryKegiatan', compact('kegiatan'));
//        return dd($kegiatan);
    }

    public function edit($id)
    {
        $kegiatan = User::find($id);
        return view('pages.opd.kegiatan.formKegiatan', compact('kegiatan'));
    }

    public function update(Request $request, $id)
    {
        $kegiatan = Kegiatan::find($id);
        $data = Kegiatan::update([
            'nama_kegiatan' => $request->NamaKegiatan,
            'tanggal_kegiatan' => $request->TanggalKegiatan,
            'lokasi' => $request->lokasi,
            'narasumber' => $request->narasumber,
            'agenda' => $request->agenda,
            'hasil' => $request->hasil,
            'opd_id' => $request->NamaOpd
        ]);

        if ($data) {
            return redirect()->route('daftar-kegiatan.index')->with('success', 'Data kegiatan berhasil diubah');
        } else {
            return redirect()->back()->with('error', 'Data kegiatan gagal diubah');
        }
    }

    public function destroy($id)
    {
        $kegiatan = Kegiatan::find($id);
        $data = Kegiatan::delete();
        if ($data) {
            return redirect()->back()->with(['success' => 'Data kegiatan berhasil dihapus']);
        } else {
            return redirect()->back()->with(['error' => 'Data kegiatan gagal dihapus']);
        }
    }

    public function showByDate(Request $request)
    {
        $kegiatans = Kegiatan::where('opd_id', '=', Auth::user()->opd->id)->where('tanggal_kegiatan', '=', $request->tanggal)->orderBy('tanggal_kegiatan', 'desc')->paginate(8);
        return view('pages.opd.kegiatan.showKegiatanByDate', compact('kegiatans'));
    }
}
