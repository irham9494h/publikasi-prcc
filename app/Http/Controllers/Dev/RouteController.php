<?php

namespace App\Http\Controllers\Dev;

use App\Opd;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

class RouteController extends Controller
{
    public function index(){
            Artisan::call('route:list');
            return '<pre>'.Artisan::output();
//        return Hash::make('sekawan!');
    }

    public function filter(){
        $opd = DB::select('select * from opds where opds.id not in (select opd_id from users) order by opds.nama asc');
        return view('route', compact('opd'));
    }
}
