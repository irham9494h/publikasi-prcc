@extends('layout.main')
@section('title','Data Users')
@section('content')
    <section class="content-header">
        <h1>
            OPD Management
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">OPD Management</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Data OPD Provinsi NTB</h3>
                        <div class="box-tools">
                            <a href="{{route('opd-manajemen.create')}}" class="btn btn-primary btn-sm"><span
                                        class="fa fa-plus"></span> Tambah Data</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                @include('layout.includes.notofication')
                            </div>
                            <div class="col-lg-12">
                                <table id="table_id" class="table table-responsive table-hover">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama OPD</th>
                                        <th>Enail</th>
                                        <th>Telepon Instansi</th>
                                        <th>Telp./HP PIC</th>
                                        <th>Jumlah User</th>
                                        <th style="">Jumlah Kegiatan</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($opds as $opd)
                                        <tr>
                                            <th scope="row">{{$loop->iteration}}</th>
                                            <td>{{$opd->nama}}</td>
                                            <td>{{$opd->email}}</td>
                                            <td>{{$opd->hp_instansi}}</td>
                                            <td>{{$opd->hp_pic}}</td>
                                            <td>
                                                <small class="label bg-primary">{{count($opd->user)}}</small>
                                            </td>
                                            <td>
                                                <small class="label bg-primary">{{count($opd->kegiatan).' '}}</small>
                                                <span>
                                                    <form action="{{route('daftar-kegiatan.show-by-opd')}}" method="GET" style="z-index: auto;margin-top: -25px;margin-left: 10px;">
                                                        @csrf
                                                        <input type="hidden" name="opd" value="{{$opd->id}}">
                                                        <button type="submit" class="btn btn-link">Lihat</button>
                                                    </form>
                                                </span>
                                            </td>
                                            <td style="width: 50px">
                                                {{--<a class="btn btn-primary btn-sm"--}}
                                                {{--href="{{route('opd-manajemen.edit', $opd->id)}}"><i--}}
                                                {{--class="fa fa-eye" title="Detail Kegiatan"></i></a>--}}
                                                <a class="btn btn-primary btn-xs"
                                                   href="{{route('opd-manajemen.edit', $opd->id)}}"><i
                                                            class="fa fa-edit" title="Edit Data"></i></a>
                                                <a class="btn btn-danger btn-xs"
                                                   href="{{route('opd-manajemen.delete', $opd->id)}}"
                                                   onclick="alert('Yakin ingin menghapus data?')"><i
                                                            class="fa fa-trash" title="Hapus Data"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
