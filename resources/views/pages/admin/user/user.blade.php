@extends('layout.main')
@section('title','Data Users')
@section('content')
    <section class="content-header">
        <h1>
            User Management
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">User Management</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @include('layout.includes.notofication')
            </div>
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Data User OPD Provinsi NTB</h3>
                        <div class="box-tools">
                            <a href="{{route('user-manajemen.create')}}" class="btn btn-primary btn-sm"><span
                                        class="fa fa-plus"></span> Tambah Data</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <table id="table_id" class="table table-responsive table-hover">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>OPD</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Username</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($managementUser as $user)
                                        <tr>
                                            <th scope="row">{{$loop->iteration}}</th>
                                            <td>{{$user->opd->nama}}</td>
                                            <td>{{$user->nama}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>{{$user->username}}</td>
                                            <td>
                                                <a href="{{route('user-manajemen.edit', $user->id)}}"
                                                   class="btn btn-primary btn-sm" type="submit" title="Edit Data"><span
                                                            class="fa fa-edit"></span></a>
                                                <a class="btn btn-danger btn-sm"
                                                   href="{{route('user-manajemen.delete', $user->id)}}"
                                                   onclick="alert('Yakin ingin menghapus data?')"><i
                                                            class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
