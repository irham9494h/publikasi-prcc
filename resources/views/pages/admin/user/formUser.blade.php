@extends('layout.main')
@section('title','Form Data Users')
@section('content')
    @if (isset($user))
        <section class="content-header">
            <h1>
                User Management
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-user"></i> Home</a></li>
                <li class="active">Update User</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('layout.includes.notofication')
                </div>
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <form action="{{ route('user-manajemen.update', $user->id) }}" method="POST">
                            <div class="box-body">
                                {{csrf_field()}}
                                {{method_field('PUT')}}
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Organisasi Perangkat Daerah</label>
                                        <select name="NamaOpd" id="" class="form-control select2" required disabled>
                                            <option value="" selected disabled="">Pilih OPD</option>
                                            @foreach($opds as $opd)
                                                <option value="{{$opd->id}}" {{$opd->id == $user->opd->id ? 'selected' : ''}} >{{$opd->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Nama</label>
                                        <input type="text" class="form-control" name="nama" value="{{$user->nama}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control" name="email" value="{{$user->email}}" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" class="form-control" name="username" value="{{$user->username}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" name="password">
                                        <p class="text-muted">Kosongkan jika tidak ingin mengubah password</p>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="button" class="btn btn-danger">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    @else
        <section class="content-header">
            <h1>
                User Management
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-user"></i> Home</a></li>
                <li class="active">Tambah User</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('layout.includes.notofication')
                </div>
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <form action="{{ route('user-manajemen.store') }}" method="POST">
                            <div class="box-body">
                                {{csrf_field()}}
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Organisasi Perangkat Daerah</label>
                                        <select name="NamaOpd" id="" class="form-control select2" required>
                                            <option value="" selected disabled="">Pilih OPD</option>
                                            @foreach($opds as $opd)
                                                <option value="{{$opd->id}}">{{$opd->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Nama</label>
                                        <input type="text" class="form-control" name="nama" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control" name="email" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" class="form-control" name="username" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" name="password" required>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="reset" class="btn btn-danger">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    @endif
@stop
