<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>RAPI | Register</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('/css/bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="{{asset('/plugin/select2/css/select2.min.css')}}">
    <style>
        small {
            font-size: 50% !important;
        }

    </style>
</head>

<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <b>RAPI</b>
        <br>
        <small>Repository Agency Public Information</small>
    </div>
    <form action="{{ route('user.register') }}" method="post">
        <div class="login-box-body">
            <p class="login-box-msg">Silahkan isikan data anda terlebih dahulu</p>
            {{csrf_field()}}
            <div class="form-group">
                <label>Organisasi Perangkat Daerah</label>
                <select name="opd_id" id="" class="form-control select2" required>
                    <option value="" selected disabled="">Pilih OPD</option>
                    @foreach($opds as $opd)
                        <option value="{{$opd->id}}">{{$opd->nama}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group has-feedback">
                <label>Nama</label>
                <input type="text" class="form-control" name="nama" required>
            </div>
            <div class="form-group has-feedback">
                <label>Email</label>
                <input type="email" class="form-control" name="email" required>
            </div>
            <div class="form-group has-feedback">
                <label>Username</label>
                <input type="text" class="form-control" name="username" required>
            </div>
            <div class="form-group has-feedback">
                <label>Password</label>
                <input type="password" class="form-control" name="password" required>
            </div>
        </div>

        <div class=" box-footer">
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Daftar</button>
            </div>
        </div>
    </form>
</div>
<script src=" {{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{ asset('/plugin/select2/js/select2.min.js')}}"></script>

<script>
    $(document).ready(function () {
        $('.select2').select2({
              width: 'resolve'
        });
    });
</script>

</body>

</html>
