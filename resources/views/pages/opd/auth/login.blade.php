<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>RAPI | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('/css/bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">

    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        small {
            font-size: 50% !important;
        }

    </style>
    <style type="text/css">
        .login-page,
        .register-page {
            /* background: #dddddd url('http://36.91.156.2:9000/vendor/crudbooster/assets/prcc.png'); */
            /*background-image: linear-gradient(to bottom right, rgba(255,201,7,0), rgba(0,159,222,0.8), rgba(32,68,150,0.8));*/
            color: #ffffff !important;
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
        }

        .login-box,
        .register-box {
            margin: 2% auto;
        }

        .login-box-body {
            box-shadow: 0px 0px 50px rgba(0, 0, 0, 0.8);
            height: 400px;
            background: rgba(255, 255, 255, 0.9);
            color: #666666 !important;

        }

        html,
        body {
            overflow: hidden;
        }

        .boxlogin {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

    </style>
</head>

<body class="login-page">

    <table class="boxlogin">
        <tbody>
            <tr>
                <td>
                    <div class="login-box" style="background-color: blue;height: 400px;">
                        <div class="login-logo">

                            <img title="AGENDA"
                                src="{{asset('img/RAPI.jpg')}}"
                                style="max-width: 100%;">

                        </div>
                    </div>

                </td>
                <td>

                    <div class="login-box">
                        <div class="login-box-body" style="padding-top: 100px">
                            <p class="login-box-msg">Silahkan login untuk masuk</p>
                            <form action="{{ route('login') }}" method="post">
                                {{csrf_field()}}
                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control" name="username" autofocus>
                                    <br>
                                    <sub>Silahkan Tuliskan <b>Email</b> Atau <b>Username</b> Anda.</sub>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="password" class="form-control" name="password">
                                    <br>
                                    <sub>Minimal 8 Karakter.</sub>
                                </div>
                                <div class="row">
                                    <div class="col-xs-8">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"> Remember Me
                                            </label>
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-xs-4">
                                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign
                                            In</button>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </form>
                            <br>
                            Belum punya akun ? Klik <a href="{{route('register')}}" class="text-center">disini</a>
                        </div>

                    </div><!-- /.login-box -->

                </td>
            </tr>

        </tbody>
    </table>




    <!-- jQuery 2.1.3 -->
    <script src="http://36.91.156.2:9000/vendor/crudbooster/assets/adminlte/plugins/jQuery/jQuery-2.1.4.min.js">
    </script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="http://36.91.156.2:9000/vendor/crudbooster/assets/adminlte/bootstrap/js/bootstrap.min.js"
        type="text/javascript"></script>


</body>

</html>
