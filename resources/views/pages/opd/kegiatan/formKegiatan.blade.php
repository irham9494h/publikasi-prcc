@extends('layout.main')
@section('title','Tambah Data Kegiatan')
@section('content')
    <section class="content-header">
        <h1>
            User Management
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Home</a></li>
            <li class="active">Tambah Kegiatan</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @include('layout.includes.notofication')
            </div>
            <div class="col-md-8 col-md-offset-2">
                <div class="box box-primary">
                    <form action="{{ route('kegiatan.store') }}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="box-body">
                            <div class="form-group">
                                <label>Nama Kegiatan <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="NamaKegiatan" required>
                            </div>
                            <div class="form-group">
                                <label>Tanggal Kegiatan <span class="text-danger">*</span></label>
                                <div class="input-group date" style="width: 100% !important;">
                                    <input type="text" class="form-control" name="tanggal" id="datepicker"
                                           style="width: 100% !important;" autocomplete="false" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Lokasi <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="lokasi" required>
                            </div>
                            <div class="form-group">
                                <label>Narasumber <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="narasumber" required>
                            </div>
                            <div class="form-group">
                                <label>Agenda <span class="text-danger">*</span></label>
                                <textarea class="form-control" name="agenda" id=""placeholder="Agenda" required></textarea>
                            </div>
                            <div class="form-group">
                                <label>Hasil <span class="text-danger">*</span></label>
                                <textarea class="form-control" name="hasil" id="" placeholder="Hasil kegiatan" required></textarea>
                            </div>
                            <div class="form-group">
                                <label>Foto Kegiatan <span class="text-danger">*</span></label>
                                <input type="file" class="form-control" name="foto_kegiatan" required>
                                <sub>Mohon upload gambar berekstensi <b class="text-red">JPG</b>, <b class="text-red">JPEG</b>, <b class="text-red">PNG</b></sub>
                            </div>
                        </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="reset" class="btn btn-danger">Reset</button>
                    </div>
                </form>
                <div class="col-lg-2"></div>
            </div>
        </div>
    </section>
@stop
