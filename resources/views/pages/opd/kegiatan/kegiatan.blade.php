@extends('layout.main')
@section('title','Daftar Kegiatan')
@section('content')
    <section class="content-header">
        <h1>
            Data Kegiatan
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Daftar Kegiatan</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Daftar Kegiatan</h3>
                        <div class="box-tools">
                            <a href="{{route('kegiatan.create')}}" class="btn btn-primary btn-sm"><span
                                        class="fa fa-plus"></span> Tambah Kegiatan</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            @foreach($kegiatans as $keg)
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header bg-info">
                            <h3 class="box-title">Kegiatan pada tanggal : <span
                                        class="text-muted">{{$keg['tanggal']}}</span></h3>
                        </div>
                        <div class="box-body row">
                            @foreach($keg['data'] as $data)
                                <div class="col-md-3">
                                    <div class="box box-solid box-default">
                                        <div class="box-header" style="padding: 0 !important;">
                                            {{--@foreach($keg->photo as $photo)--}}
                                            {{--<img src="{{asset('storage/'.$photo->thumbnail)}}"--}}
                                            {{--alt="" width="100%">--}}
                                            {{--@endforeach--}}
                                        </div>
                                        <div class="box-body">
                                            <div class="title-head"
                                                 style="border-bottom: 1px solid #f4f4f4; padding-bottom: 5px">
                                                @if(strlen($data->nama_kegiatan) >= 30)
                                                    {{substr($data->nama_kegiatan, 0, 30)."..."}}
                                                @else
                                                    {{$data->nama_kegiatan}}
                                                @endif
                                            </div>
                                            <sub class="text-muted"><i class="fa fa-clock-o"></i>
                                                <span>{{$data->tanggal_kegiatan}}</span></sub>
                                            <sub class="text-muted"><i class="fa fa-map-pin"></i>
                                                <span>{{$data->lokasi}}</span></sub>
                                        </div>
                                        <div class="box-footer">
                                            <a href="{{route('kegiatan.show', $data->id)}}"
                                               class="btn btn-block btn-default btn-sm btn-flat"><i class="fa fa-eye"></i> Detail</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="col-md-12" style="border-top: 1px solid #f4f4f4; padding-bottom: 5px; margin-bottom: -12px">
                                <form action="{{route('kegiatan.show-by-date')}}" method="get">
                                    @csrf
                                    <input type="hidden" name="tanggal" value="{{$keg['tanggal']}}">
                                    <button type="submit" class="btn btn-link pull-right">Lihat semua kegiatan <i class=" fa fa-arrow-right"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="text-center">{{$res->links()}}</div>
        </div>
    </section>
@stop
