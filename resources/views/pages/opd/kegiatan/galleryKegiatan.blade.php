@extends('layout.main')
@section('title','Gallery Kegiatan')
@section('content')
    <section class="content-header">
        <h1>
            Detail Kegiatan
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Home</a></li>
            <li class="active">Kegiatan</li>
            <li class="active">{{$kegiatan->nama_kegiatan}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-10">
                <div class="box box-widget">
                    <div class="box-header with-border" style="padding:0 !important;">

                    </div>
                    <div class="box-body">
                        <span class="username">
                            <h3>{{$kegiatan->nama_kegiatan}}</h3>
                        </span>
                        <br>
                        <span class="description"><i class="fa fa-map-pin"></i> {{$kegiatan->lokasi}} - <i
                                    class="fa fa-clock-o"></i> {{$kegiatan->tanggal_kegiatan}}</span>
                        <hr>
                        <h4>Narasumber : <span>{{$kegiatan->narasumber}}</span></h4>
                        <h4>Agenda : </h4>
                        <div class="callout" style="background-color: #EEEEEE">
                            <p>{{$kegiatan->agenda}}</p>
                        </div>
                        <h4>Hasil :</h4>
                        <div class="callout callout-info">
                            <p>{{$kegiatan->hasil}}</p>
                        </div>
                    </div>
                    <div class="box-footer">
                        @foreach($kegiatan->photo as $photo)
                            <a href="{{asset('storage/'.$photo->foto)}}" download="" class="btn btn-primary btn-sm"><i class="fa fa-download"></i> Download Gambar</a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-lg-1"></div>
        </div>
    </section>
@stop
