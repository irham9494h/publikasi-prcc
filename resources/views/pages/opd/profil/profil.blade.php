@extends('layout.main')
@section('title','Profil OPD')
@section('profil-active', 'active')
@section('content')
    <section class="content-header">
        <h1>
            Profil OPD
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Profil OPD</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @include('layout.includes.notofication')
            </div>
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Profil OPD</h3>
                        <div class="box-tools">
                            <a href="{{route('opd.edit-profile', $profil->id)}}" class="btn btn-primary btn-sm"><span
                                        class="fa fa-edit"></span> Ubah Profil OPD</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Nama OPD</b> <a class="pull-right">{{$profil->nama}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Email</b> <a class="pull-right">{{$profil->email}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Facebook</b> <a class="pull-right">{{$profil->facebook}}</a>
                            </li>

                            <li class="list-group-item">
                                <b>Instagram</b> <a class="pull-right">{{$profil->instagram}}</a>
                            </li>

                            <li class="list-group-item">
                                <b>Twitter</b> <a class="pull-right">{{$profil->twitter}}</a>
                            </li>

                            <li class="list-group-item">
                                <b>Telegram</b> <a class="pull-right">{{$profil->telegram}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Telepon/HP Instansi</b> <a class="pull-right">{{$profil->hp_instansi}}</a>
                            </li>

                            <li class="list-group-item">
                                <b>HP PIC</b> <a class="pull-right">{{$profil->hp_pic}}</a>
                            </li>

                            <li class="list-group-item">
                                <b>Logo Instansi</b>
                                <br>
                                <img src="{{asset('storage/'.$profil->foto_pic)}}" alt="" width="100px">
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
