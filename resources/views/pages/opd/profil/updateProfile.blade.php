@extends('layout.main')
@section('title','Tambah Data Users')
@section('profil-active', 'active')

@section('content')
    <section class="content-header">
        <h1>
            OPD Management
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Home</a></li>
            <li class="active">Tambah OPD</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-solid">
                    <div class="box-body">
                        <a href="{{route('kegiatan.index')}}" class="pull-right btn btn-success btn-sm"><span
                                    class="fa fa-arrow-left"></span> Kembali</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @include('layout.includes.notofication')
            </div>
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <form action="{{route('opd.update-profile', $opd->id)}}" method="POST"
                              enctype="multipart/form-data">
                            {{csrf_field()}}
                            {{method_field('PUT')}}
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Nama OPD <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Nama OPD" name="nama"
                                               value="{{$opd->nama}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Email <span class="text-danger">*</span></label>
                                        <input type="email" class="form-control" name="email"
                                               placeholder="Email OPD" value="{{$opd->email}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Facebook</label>
                                        <input type="text" class="form-control" name="facebook"
                                               placeholder="Facebook OPD" value="{{$opd->facebook}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Instagram</label>
                                        <input type="text" class="form-control" name="instagram"
                                               placeholder="Instagram OPD" value="{{$opd->instagram}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Twitter</label>
                                        <input type="text" class="form-control" name="twitter"
                                               placeholder="Twitter OPD" {{$opd->twitter}}>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Telegram</label>
                                        <input type="text" class="form-control" name="telegram"
                                               placeholder="Telegram OPD" {{$opd->telegram}}>
                                    </div>
                                    <div class="form-group">
                                        <label>Telepon/HP OPD <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="hp_instansi"
                                               placeholder="Nomor Telepon OPD" value="{{$opd->hp_instansi}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Alamat OPD <span class="text-danger">*</span></label>
                                        <textarea class="form-control" name="alamat" id=""
                                                  placeholder="Alamat OPD" required>{{$opd->alamat}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Telepon/HP PIC <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="hp_pic"
                                               placeholder="Nomor Telepon PIC" value="{{$opd->hp_pic}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Logo Instansi</label>
                                        <input type="file" name="image" id="image" class="form-control">
                                        <p class="text-muted">Kosongkan jika tidak ingin mengubah foto</p>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="reset" class="btn btn-danger">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
