<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Menu Utama</li>
            @if(Auth::user()->role == 'admin')
                <li class="{{ (strpos(Route::currentRouteName(), 'opd-manajemen') === 0) ? 'active' : '' }}"><a
                            href="{{ route('opd-manajemen.index')}}"><i class="fa fa-dot-circle-o"></i> <span>Management
                        OPD</span></a></li>
                <li class="{{ (strpos(Route::currentRouteName(), 'user-manajemen') === 0) ? 'active' : '' }}"><a
                            href="{{ route('user-manajemen.index')}}"><i class="fa fa-dot-circle-o"></i> <span>Management
                        User</span></a></li>
                <li class="{{ (strpos(Route::currentRouteName(), 'daftar-kegiatan') === 0) ? 'active' : '' }}"><a
                            href="{{ route('daftar-kegiatan.index')}}"><i class="fa fa-dot-circle-o"></i> <span>Data
                        Kegiatan</span></a></li>
            @else
                <li class="@yield('profil-active')"><a
                            href="{{ route('opd.profile', Auth::user()->opd->id)}}"><i class="fa fa-dot-circle-o"></i> <span>Profil OPD</span></a>
                </li>
                <li class="{{ (strpos(Route::currentRouteName(), 'kegiatan') === 0) ? 'active' : '' }}"><a
                            href="{{ route('kegiatan.index')}}"><i class="fa fa-dot-circle-o"></i> <span>Kegiatan</span></a>
                </li>
            @endif
        </ul>
    </section>
</aside>
