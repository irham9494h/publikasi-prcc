<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>RAPI | @yield('title')</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="stylesheet" href="{{asset('/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('/plugin/Ionicons/css/ionicons.min.css')}}">
<link rel="stylesheet" href="{{asset('/plugin/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('/css/AdminLTE.min.css')}}">
<link rel="stylesheet" href="{{asset('/plugin/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('/plugin/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('/plugin/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('/css/AdminLTE.min.css')}}">
<link rel="stylesheet" href="{{asset('/css/skins/_all-skins.min.css')}}">
<link rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">