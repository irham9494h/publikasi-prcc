<!-- jQuery 3 -->
<script src="{{asset('js/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('js/adminlte.min.js')}}"></script>
<!-- DataTables -->
<script src="{{ asset('/plugin/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('/plugin/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{ asset('/plugin/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ asset('/plugin/select2/js/select2.min.js')}}"></script>

<script>
    $(document).ready(function () {
        $('#table_id').DataTable({
            responsive: true
        });

        $('#datepicker').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy'
        });

        $('.select2').select2({
              width: 'resolve'
        });
    });
</script>

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#profile-img").change(function(){
        readURL(this);
    });
</script>
