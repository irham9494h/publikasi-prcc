<!DOCTYPE html>
<html>

<head>
    @include('layout.includes.head')
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        @include('layout.includes.header')
        @include('layout.includes.sidebar')
        <div class="content-wrapper">
        @yield('content')
        </div>
        @include('layout.includes.footer')
    </div>
    @include('layout.includes.foot')
</body>

</html>
